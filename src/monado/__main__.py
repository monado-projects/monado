import typer

from monado import (
    __version__,
)
from monado.utils.cli import (
    VersionOption,
)

app = typer.Typer(
    invoke_without_command=True,
    no_args_is_help=True,
)


@app.callback()
def cli_callback(
    version: bool = VersionOption(__version__),
) -> None:
    pass


@app.command()
def about() -> None:
    typer.echo(f"Monado CLI version {__version__}")


if __name__ == "__main__":
    app()
