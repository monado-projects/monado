from typing import (
    Any,
)

import typer


def VersionOption(  # noqa: N802
    version: str,
) -> Any:
    """
    Display version of the application and exit.
    """

    def _handle_option(value: bool) -> bool:
        if value:
            typer.echo(version)
            raise typer.Exit()
        return value

    return typer.Option(
        False,
        callback=_handle_option,
    )
