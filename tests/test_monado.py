from typer.testing import (
    CliRunner,
)

from monado import (
    __version__,
)
from monado.__main__ import (
    app,
)

runner = CliRunner()


def test_about() -> None:
    result = runner.invoke(app, ["about"])
    assert result.exit_code == 0
    assert __version__ in result.stdout
    assert "Monado" in result.stdout


def test_version() -> None:
    result = runner.invoke(app, ["--version"])
    assert result.exit_code == 0
    assert __version__ == result.stdout.strip()
